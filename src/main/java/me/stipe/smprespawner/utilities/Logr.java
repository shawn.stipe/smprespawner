package me.stipe.smprespawner.utilities;

import org.apache.logging.log4j.Logger;
import org.bukkit.plugin.Plugin;

public class Logr implements ConfigSettings {
    private final Logger logger;
    @ConfigSetting
    protected boolean debug = false;

    public Logr(Plugin plugin) {
        logger = plugin.getLog4JLogger();
    }

    /**
     * @param message message to log with String.format compatible placeholders
     * @param args the arguments
     */
    public void log(String message, Object... args) {
        logger.info(String.format(message, args));
    }

    public void debug(String message, Object... args) {
        if (debug)
            logger.info(String.format(message, args));
    }
}
