package me.stipe.smprespawner.modules;

import me.stipe.smprespawner.utilities.ConfigSetting;
import me.stipe.smprespawner.utilities.ConfigSettings;
import me.stipe.smprespawner.utilities.Logr;
import org.bukkit.Location;
import org.bukkit.StructureType;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Shulker;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import java.util.concurrent.ThreadLocalRandom;

public class ShulkerRespawner implements Listener, ConfigSettings {
    private final Logr logr;
    @ConfigSetting
    protected boolean respawnShulkers = true;
    @ConfigSetting
    protected int dontSpawnIfShulkerIsWithinRadius = 12;
    @ConfigSetting
    protected boolean onlySpawnInEndCities = true;
    @ConfigSetting
    protected double probabilityOfSpawningInPlaceOfEnderman = 0.5;
    @ConfigSetting
    protected int cooldownBetweenShulkerSpawnInMinutes = 5;
    private long lastRespawn;

    public ShulkerRespawner(Logr logr) {
        this.logr = logr;
        lastRespawn = System.currentTimeMillis();
    }

    @EventHandler (ignoreCancelled = true)
    public void respawnShulkers(EntitySpawnEvent event) {
        if (!respawnShulkers)
            return;

        Entity e = event.getEntity();

        if (isNearEndCity(e.getLocation()) || !onlySpawnInEndCities) {
            if (event.getEntity().getType() != EntityType.ENDERMAN)
                return;

            Block b = e.getLocation().getBlock();

            if (b.getType().toString().contains("PURPUR") || b.getRelative(BlockFace.DOWN).getType().toString().contains("PURPUR")) {
                Location loc = event.getLocation();
                double roll = ThreadLocalRandom.current().nextDouble();


                if (roll <= probabilityOfSpawningInPlaceOfEnderman && loc.getNearbyEntitiesByType(Shulker.class, dontSpawnIfShulkerIsWithinRadius).isEmpty()) {
                    if (System.currentTimeMillis() - lastRespawn < cooldownBetweenShulkerSpawnInMinutes * 60L * 1000)
                        return;
                    else
                        lastRespawn = System.currentTimeMillis();
                    event.setCancelled(true);
                    loc.getWorld().spawnEntity(loc, EntityType.SHULKER);
                    logr.debug("Spawned new Shulker at (%.0f, %.0f, %.0f) in %s", loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName());
                }
            }
        }
    }

    private boolean isNearEndCity(Location location) {
        Biome biome = location.getBlock().getBiome();

        if (biome == Biome.END_HIGHLANDS || biome == Biome.END_MIDLANDS) {
            Location loc = location.getWorld().locateNearestStructure(location, StructureType.END_CITY, 5, false);

            return loc != null && loc.distance(location) <= 100;
        }
        return false;
    }
}
