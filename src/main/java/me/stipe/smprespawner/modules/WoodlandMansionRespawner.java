package me.stipe.smprespawner.modules;

import me.stipe.smprespawner.utilities.ConfigSetting;
import me.stipe.smprespawner.utilities.ConfigSettings;
import me.stipe.smprespawner.utilities.Logr;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.StructureType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Evoker;
import org.bukkit.entity.Vindicator;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class WoodlandMansionRespawner implements Listener, ConfigSettings {
    private final static int MANSION_SCAN_RANGE = 100;
    private final Logr logr;
    private final Map<EntityType, List<Long>> respawns;
    @ConfigSetting
    protected boolean respawnMansions = true;
    @ConfigSetting
    protected int evokerLimit = 3;
    @ConfigSetting
    protected int vindicatorLimit = 15;
    @ConfigSetting
    protected int mansionRespawnCooldownInMinutes = 480;

    public WoodlandMansionRespawner(Logr logr) {
        this.logr = logr;
        respawns = new HashMap<>();
        respawns.put(EntityType.EVOKER, new ArrayList<>());
        respawns.put(EntityType.VINDICATOR, new ArrayList<>());
    }

    @EventHandler (ignoreCancelled = true)
    public void respawnMansionMobs(EntitySpawnEvent event) {
        if (event.getEntity().getType() == EntityType.EVOKER || event.getEntity().getType() == EntityType.VINDICATOR)
            return;

        if (respawnMansions && isNearbyMansion(event.getLocation()) && isGoodSpawnBlock(event.getLocation().getBlock())) {
            EntityType type;
            int limit;
            int total = evokerLimit + vindicatorLimit;

            if (ThreadLocalRandom.current().nextInt(0, total) > evokerLimit) {
                type = EntityType.VINDICATOR;
                limit = vindicatorLimit;
            } else {
                type = EntityType.EVOKER;
                limit = evokerLimit;
            }

            if (hasCooldown(type))
                return;

            Location loc = event.getLocation();

            if (countNearby(type, loc) >= limit)
                return;

            event.setCancelled(true);
            loc.getWorld().spawnEntity(loc, type);
            respawns.get(type).add(System.currentTimeMillis());
            logr.log("Spawned new %s at (%.0f, %.0f, %.0f)", type.name().toLowerCase(), loc.getX(), loc.getY(), loc.getZ());
        }
    }

    private boolean isNearbyMansion(Location location) {
        Location nearest = location.getWorld().locateNearestStructure(location, StructureType.WOODLAND_MANSION, 60, false);
        return nearest != null && !(nearest.distance(location) > MANSION_SCAN_RANGE);
    }

    private boolean isGoodSpawnBlock(Block b) {
        return b.getType() == Material.BIRCH_PLANKS || b.getRelative(BlockFace.DOWN).getType() == Material.BIRCH_PLANKS;
    }

    private int countNearby(EntityType type, Location location) {
        if (type == EntityType.EVOKER)
            return location.getNearbyEntitiesByType(Evoker.class, MANSION_SCAN_RANGE).size();
        if (type == EntityType.VINDICATOR)
            return location.getNearbyEntitiesByType(Vindicator.class, MANSION_SCAN_RANGE).size();
        return 0;
    }

    /**
     * @param type entity type to check for cooldown
     * @return true if the max limit for this type has been exceeded within the cooldown period
     */
    private boolean hasCooldown(EntityType type) {
        // calculate the earliest time that it outside of cooldown range
        long earliestInMillis = System.currentTimeMillis() - mansionRespawnCooldownInMinutes * 60L * 1000L;
        int count = 0;

        // get all of the respawn times for this entity type
        for (long time : respawns.get(type)) {
            // count all of the ones that are later than our earliest time
            if (time >= earliestInMillis)
                count++;
        }

        logr.debug("Found %s %s respawns within %s minutes", count, type.name(), mansionRespawnCooldownInMinutes);

        // see if we've reached the limit and return
        if (type == EntityType.EVOKER)
            return count >= evokerLimit;
        if (type == EntityType.VINDICATOR)
            return count >= vindicatorLimit;
        return true;
    }
}
