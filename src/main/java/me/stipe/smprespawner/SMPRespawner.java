package me.stipe.smprespawner;

import me.stipe.smprespawner.modules.ShulkerRespawner;
import me.stipe.smprespawner.modules.WoodlandMansionRespawner;
import me.stipe.smprespawner.utilities.ConfigSettings;
import me.stipe.smprespawner.utilities.Logr;
import me.stipe.smprespawner.utilities.PluginSettings;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

@SuppressWarnings("unused")
public final class SMPRespawner extends JavaPlugin {
    private PluginSettings settings;

    @Override
    public void onEnable() {
        Logr logr = new Logr(this);
        settings = new PluginSettings(logr);

        initialize(logr);
        initialize(new ShulkerRespawner(logr));
        initialize(new WoodlandMansionRespawner(logr));

        // do all module initializations first
        settings.loadFromConfig(getConfig());
        settings.writeSettingsToConfig(getConfig());
        saveConfig();
    }

    /**
     * Initializes a module with the settings manager and registers listeners if there are any
     * @param module the plugin module that contains configuration settings
     */
    private void initialize(ConfigSettings module) {
        try {
            settings.attach(module);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (module instanceof Listener listener) {
            getServer().getPluginManager().registerEvents(listener, this);
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        settings.writeSettingsToConfig(getConfig());
        HandlerList.unregisterAll(this);
        saveConfig();
    }

}
